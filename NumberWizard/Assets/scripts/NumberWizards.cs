﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizards : MonoBehaviour {
	int max;
	int min;
	int guess;

	// Use this for initialization
	void Start () {
		StartGame();
	}
	
	// Update is called once per frame
	void Update (){
		verify();
	}

	void StartGame(){
		max = 1000;
		min = 1;
		guess = 500;

		print("=================================");
		print("Welcome aboard to Number Wizard!");
		print("Choose a number in your head, but dont tell me!");
		print("The highest number we can pick is " + max);
		print("The lowest number we can pick is " + min);

		makeAGuess();
		max++;
	}

	void makeAGuess (){
		guess = (max + min)/2;
		print("Is your number higher or lower than the " + guess + "?");
		print("Up - Higher\t Down - Lower\t Return - Equal");
	}

	void verify (){
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			max = guess;
			makeAGuess();
		} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
			min = guess;
			makeAGuess();
		} else if (Input.GetKeyDown (KeyCode.Return)) {
            print("Yay! I got it!");
			StartGame();
		}
	}
}
