﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NumberWizard : MonoBehaviour {

	int max;
	int min;
	int guess;
	public int maxGuessesAllowed = 5;
	public Text text;

	// Use this for initialization
	void Start () { StartGame(); }

	void StartGame(){

		max = 1000;
		min = 1;
		NextGuess();

	}

	public void GuessHigher(){

		min = guess;

		if (min == 1000) { SceneManager.LoadScene("Lose"); }
		else NextGuess();
	}

	public void GuessLower(){

		max = guess;

		if (max == 1) {	SceneManager.LoadScene("Lose");	}
		else NextGuess();
	}

	void NextGuess(){

		guess = Random.Range(min, max + 1);
		maxGuessesAllowed = maxGuessesAllowed - 1;
		text.text = guess.ToString();

		if (min == max) { SceneManager.LoadScene("Lose"); }
		if(maxGuessesAllowed <= 0){	SceneManager.LoadScene("Win"); }
	}
}
