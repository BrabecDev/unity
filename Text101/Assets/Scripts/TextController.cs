using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

	public Text text;

	private enum States	{start, cell, mirror, sheets_0, lock_0, cell_mirror, sheets_1, lock_1, corridor_0, 
						freedom, stairs_0, closet_door, floor};

	private States myState;

	// Use this for initialization
	void Start () {
		//First Scene - The Cell
		myState = States.start;
	}

	// Update is called once per frame
	void Update ()
	{
		print(myState);

		switch (myState) {
			case States.start:			start ();			break;
			case States.cell: 			cell ();			break;
			case States.sheets_0: 		sheets_0 ();		break;
			case States.lock_0:			lock_0 ();			break;
			case States.mirror:			mirror ();			break;
			case States.cell_mirror:	cell_mirror ();		break;
			case States.sheets_1:		sheets_1 ();		break;
			case States.lock_1:			lock_1 ();			break;
			case States.corridor_0:		corridor_0 ();		break;
			case States.freedom:		freedom ();			break;
		}
	}

	void start () {
		text.text = "Pressione qualquer botão para iniciar o jogo.";
		if (Input.anyKeyDown) {myState = States.cell;}
	}

	#region State handler methods
	void cell(){
		text.text = "Você é um prisioneiro e precisa escapar de sua cela." +
					" os itens ao seu dispor são: lençóis da cama, um espelho na parede" +
					" e uma porta trancada pelo lado de fora.\n\n"+
					"Pressione S para usar os lençóis\n"+
					"Pressione L para ver a porta\n"+
					"Pressione M para usar o espelho";
		if 		(Input.GetKeyDown(KeyCode.S)) {myState = States.sheets_0;}
		else if (Input.GetKeyDown(KeyCode.L)) {myState = States.lock_0;}
		else if (Input.GetKeyDown(KeyCode.M)) {myState = States.mirror;}
	}

	void sheets_0(){
		text.text = "Você mal pode acreditar que dorme em lençóis tão sujos.\n\n" +
					"Pressione R para retornar.";
		if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell;}
	}

	void lock_0(){
		text.text = "Uma porta de madeira com uma pequena fresta de frente para uma parede de concreto.\n"+
					"Parece que a porta está trancada pelo lado de fora.\n\n" +
					"Pressione R para retornar.";
		if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell;}
	}

	void mirror(){
		text.text = "Um espelho, com um cabo fino pendurado na parede da cela.\n"+
					"Você repara na sua magreza, as refeições são horríveis pelo visto.\n\n"+
					"Pressione T para pegar o espelho.\n"+
					"Pressione R para retornar.";
		if 		(Input.GetKeyDown(KeyCode.R)) {myState = States.cell;}
		else if (Input.GetKeyDown(KeyCode.T)) {myState = States.cell_mirror;}
	}

	void cell_mirror(){
		text.text = "Você está com o espelho em mãos. Mas este momento não é muito propício para reflexões, fuja!\n\n"+
					"Pressione S para ver os lençóis.\n"+
					"Pressione L para ir até a porta.";
		if 	    (Input.GetKeyDown(KeyCode.S)) {myState = States.sheets_1;}
		else if (Input.GetKeyDown(KeyCode.L)) {myState = States.lock_1;}
	}

	void sheets_1(){
		text.text = "Os lençóis não serão de grande ajuda por ora.\n\n" +
					"Pressione R para retornar.";
		if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell_mirror;}
	}

	void lock_1(){
		text.text = "Você nota que o cabo do espelho tem um formato parecido com a fechadura da porta.\n\n"+
					"Pressione O para abrir a porta por lockpicking\n"+
					"Pressione R para retornar";
		if  	(Input.GetKeyDown(KeyCode.O)) {myState = States.corridor_0;}
		else if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell_mirror;}
	}

	void corridor_0(){
		text.text = "Você conseguiu abrir a porta!\n"+
					"Agora você está no meio corredor do calabouço, está escuro e você vê uma escada. "+
					"Há também um guarda-roupa esquisito ao lado dos primeiros degraus.\n\n"+
					"Pressione S para subir as escadas\n"+
					"Pressione F para ver o chão\n"+
					"Pressione C para olhar o guarda-roupa";
		if  	(Input.GetKeyDown(KeyCode.S)) {myState = States.stairs_0;}
		else if (Input.GetKeyDown(KeyCode.F)) {myState = States.floor;}
		else if (Input.GetKeyDown(KeyCode.C)) {myState = States.closet_door;}
	}
	//This ends the First Scene, but I'm adding more complexity.
	void freedom(){
		text.text = "Você conseguiu abrir a porta! Run to the hills!\n\n"+
					"Pressione P para jogar novamente";
		if (Input.GetKeyDown(KeyCode.P)) {myState = States.cell;}
	}
	#endregion
}