﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {
	//Since it's a static variable, it won't be created more than once.
	static MusicPlayer instance = null;

	void Awake(){
		
		Debug.Log ("Music player awake " + GetInstanceID());

		//First instance of the background music
		if (instance == null) {
			
			instance = this; //It claims the instance of the Class defining it as this gameObject(the music)
			GameObject.DontDestroyOnLoad(gameObject);

		}else{
			
			Destroy (gameObject);
			print ("Duplicate music player self-destructing!");

		}
	}

	void Start(){
		
		Debug.Log ("Music player start " + GetInstanceID());

	}
}
